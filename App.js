import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import firebase from 'firebase';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
//import LoginForm from './src/components/LoginForm';
import Router from './src/Router';
//import Asd from './src/Asd';

class App extends Component {
  componentWillMount() {
    firebase.initializeApp({
    apiKey: 'AIzaSyAbQCSQOJz_C3khsVD3iFJSFKJErOnLXqY',
    authDomain: 'authentication-bac4b.firebaseapp.com',
    databaseURL: 'https://authentication-bac4b.firebaseio.com',
    projectId: 'authentication-bac4b',
    storageBucket: 'authentication-bac4b.appspot.com',
    messagingSenderId: '544705012026'
  });
  }
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
  return (
    <Provider store={store}>
    <View style={{ flex: 1 }}>
      <Router />
    </View>
    </Provider>
  );
}
}
export default App;
