import React from 'react';
import { Router, Scene, Actions } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import StudentsList from './components/StudentsList';
import StudentCreate from './components/StudentCreate';


const RouterComponent = () => {
  return (
    <Router sceneStyle={{ marginTop: 65 }}>
      <Scene key="kimlik">
    <Scene key="loginScreen" component={LoginForm} title="Giriş Ekranı" />
    </Scene>
      <Scene key="main">
        <Scene
        onRight={() => Actions.StudentCreate()}
        rightTitle="Yeni"
        key="studentsList"
        component={StudentsList}
        title="Öğrenciler"
        />
        <Scene
          key="StudentCreate"
          component={StudentCreate}
          title="Öğrenci Kaydet"
        />
      </Scene>


    </Router>
  );
};

export default RouterComponent;
