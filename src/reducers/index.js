import { combineReducers } from 'redux';
import kimlikreducers from './kimlikreducers';
import StudentListReducer from './StudentCreateReducer';
import StudentDataReducers from './StudentDataReducers';

export default combineReducers({
kimlikdogrulamaResponse: kimlikreducers,
studentsListResponse: StudentListReducer,
studentDataResponse: StudentDataReducers
});
